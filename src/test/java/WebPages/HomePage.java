package WebPages;

import WebUtils.ElementImplement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class HomePage {

    private WebDriver driver;
    ElementImplement webElementOperations =  new ElementImplement();

    /**
     * Declaring Constructor
     *
     * @param driver
     */
    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    /**
     * Click on Sign Up Button
     *
     * @author neham
     */
    public void clickSignUpButton(){
        webElementOperations.click(driver,1000,btnCreateAccount);
    }

    /**
     * Click on Login Button on Header
     *
     * @author neham
     */
    public void clickHeaderLoginButton(){
        webElementOperations.click(driver,1000,btnHeaderSignIn);
    }

    /**
     * Verify button My Wishlists after login
     *
     * @author neham
     */
    public boolean verifyMyWishlistsButton(){
       return btnMyWishLists.isDisplayed();
    }

    /**
     * Click on Login Button
     *
     * @author neham
     */
    public void clickLoginButton(){
        webElementOperations.click(driver,1000,btnSignIn);
    }
    
    @FindBy(xpath = "//button[@id='SubmitCreate']")
    WebElement btnCreateAccount;
    @FindBy(xpath = "//button[@id='SubmitLogin']")
    WebElement btnSignIn;
    @FindBy(xpath = "//a[@title='Log in to your customer account']")
    WebElement btnHeaderSignIn;
    @FindBy(xpath = "//i[@class='icon-heart']")
    WebElement btnMyWishLists;
}
