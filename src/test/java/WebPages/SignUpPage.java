package WebPages;

import WebUtils.ElementImplement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


public class SignUpPage {

    private WebDriver driver;
    ElementImplement webElementOperations =  new ElementImplement();

    /**
     * Declaring Constructor
     *
     * @param driver
     */
    public SignUpPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    /**
     * Click on Sign Up Button
     *
     * @author neham
     */
    public void enterSignUpDetails(String firstName, String lastName, String address, String city, String postcode, String state, String phone, String password) throws InterruptedException {
        webElementOperations.sendText(driver,1000,txtFirstName,firstName);
        webElementOperations.sendText(driver,1000,txtLastName,lastName);
        webElementOperations.sendText(driver,1000,txtAddress,address);
        webElementOperations.sendText(driver,1000,txtCity,city);
        webElementOperations.sendText(driver,1000,txtPostCode,postcode);
        webElementOperations.selectValueFromDropdown(driver, "//select[@name='id_state']", state);
        webElementOperations.sendText(driver,1000,txtPhone,phone);
        webElementOperations.sendText(driver,1000,txtPassword,password);
        webElementOperations.click(driver,1000,btnRegister);
    }

    //Elements
    @FindBy(xpath = "//input[@name='customer_firstname']")
    WebElement txtFirstName;
    @FindBy(xpath = "//input[@name='customer_lastname']")
    WebElement txtLastName;
    @FindBy(xpath = "//input[@name='address1']")
    WebElement txtAddress;
    @FindBy(xpath = "//input[@name='email_create']")
    WebElement txtEmail;
    @FindBy(xpath = "//input[@name='city']")
    WebElement txtCity;
    @FindBy(xpath = "//input[@name='postcode']")
    WebElement txtPostCode;
    @FindBy(xpath = "//select[@name='id_country']")
    WebElement drpCountry;
    @FindBy(xpath = "//input[@name='passwd']")
    WebElement txtPassword;
    @FindBy(xpath = "//input[@name='phone_mobile']")
    WebElement txtPhone;
    @FindBy(xpath = "//span[@class='ladda-label' and text()='Signup']")
    WebElement btnSignUp;
    @FindBy (xpath = "//button[@name='submitAccount']")
    WebElement btnRegister;
}
