package WebPages;

import WebUtils.ElementImplement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AuthenticationPage {
    private WebDriver driver;
    ElementImplement webElementOperations =  new ElementImplement();

    /**
     * Declaring Constructor
     *
     * @param driver
     */
    public AuthenticationPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    /**
     * Click on Sign Up Button
     *
     * @author neham
     */
    public void enterEmailAndClickCreateAccount(String email){
        webElementOperations.sendText(driver,1000,txtEmail,email);
        btnCreateAccount.click();
    }
    /**
     * Login with user credentials
     *
     * @author neham
     */
    public void enterDetailsAndLogin(String email, String password){
        webElementOperations.sendText(driver,1000,txtLoginEmail,email);
        webElementOperations.sendText(driver,1000,txtLoginPasswd,password);
        btnSignIn.click();
    }

    //Elements
    @FindBy(xpath = "//input[@name='email_create']")
    WebElement txtEmail;
    @FindBy(xpath = "//input[@name='email']")
    WebElement txtLoginEmail;
    @FindBy(xpath = "//input[@name='passwd']")
    WebElement txtLoginPasswd;
    @FindBy(xpath = "//button[@id='SubmitCreate']")
    WebElement btnCreateAccount;
    @FindBy(xpath = "//button[@id='SubmitLogin']")
    WebElement btnSignIn;
}
