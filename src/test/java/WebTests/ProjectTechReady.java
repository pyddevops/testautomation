package WebTests;

import WebPages.*;
import WebUtils.ElementImplement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class ProjectTechReady extends BaseClass {

    ElementImplement ele = new ElementImplement();
    String firstName = "FirstName";
    String lastName = "LastName";
    String address = "Test Lane";
    String email = "POC_TechReady" + java.time.LocalDateTime.now().toString().replace(":", "_").replace(".", "_").replace("-", "_") + "@mailinator.com";
    String password = "Password1";
    String postcode = "20001";
    String city = "testCity";
    String state = "Alaska";
    String phone = "1234567890";
    String loginEmail = "testuser@testuser.com";
    String loginPassword = "Password1";


    /**
     * Testing the Sign Up functionality
     *
     * @author neham
     * @throws InterruptedException
     */
    @Test
    public void testSignUp() throws InterruptedException {
        //Navigate to the URL
        ele.navigateToURL(driver, "http://automationpractice.com/index.php");

        //Click the Sign In Button on Header
        HomePage homePage = new HomePage(driver);
        homePage.clickHeaderLoginButton();

        //Click the Sign Up Button
        AuthenticationPage authPage = new AuthenticationPage(driver);
        authPage.enterEmailAndClickCreateAccount(email);

        //Enter the Sign Up details
        SignUpPage signUp = new SignUpPage(driver);
        signUp.enterSignUpDetails(firstName, lastName, address, city, postcode, state, phone, password);

        //Asserting the First Name displayed on the Dashboard
        Assert.assertTrue(homePage.verifyMyWishlistsButton());
    }

    /**
     * Testing the Login functionality
     *
     * @author neham
     */
    @Test
    public void testLogin() {
        //Navigate to the URL
        ele.navigateToURL(driver, "http://automationpractice.com/index.php");

        //Click the Sign In Button on Header
        HomePage homePage = new HomePage(driver);
        homePage.clickHeaderLoginButton();

        //Enter details and Login Button
        AuthenticationPage authPage = new AuthenticationPage(driver);
        authPage.enterDetailsAndLogin(loginEmail, loginPassword);

        //Asserting the First Name displayed on the Dashboard
        Assert.assertTrue(homePage.verifyMyWishlistsButton());
    }


    /**
     * Tear Down method
     *
     * @author neham
     */
    @AfterMethod
    public void teardown() {
        driver.quit();
    }
}
