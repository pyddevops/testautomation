package WebTests;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.util.HashMap;
import java.util.logging.Level;

public class BaseClass {

    public static WebDriver driver ;
    WebDriver currentDriver;
    String driverPath =  "./src/test/resources/drivers/";
    String extension = ".exe";


    @BeforeMethod
    public void setup() throws Exception {
        System.setProperty("webdriver.chrome.driver", new File(driverPath + "chromedriver" + extension).getCanonicalPath());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-infobars");
        options.addArguments("--disable-notifications");
        options.setCapability( "goog:loggingPrefs", getLogPrefs());
        HashMap<String, Object> prefs = new HashMap<>();
        prefs.put("profile.default_content_setting_values.media_stream_mic", 1);
        options.setExperimentalOption("prefs", prefs);
        options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE); //To avoid NoAlerTException by changing default behaviour of the driver
        currentDriver = new ChromeDriver(options);
        //LogResults.logStep().log(LogStatus.INFO, "Open Browser ", "   Chrome");
        currentDriver.manage().window().setSize(new Dimension(1024, 768));
        currentDriver.manage().window().maximize();
        currentDriver.manage().deleteAllCookies();
        driver = currentDriver;
    }

    /**
     * Method returns the LoggingPreferences
     */
    public static LoggingPreferences getLogPrefs() {
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable( LogType.PERFORMANCE, Level.ALL );
        return logPrefs;
    }
}