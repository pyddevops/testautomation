package WebUtils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class WaitUtils implements waitInterface{

    
    public void implicitWait(WebDriver driver, int timeout){
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.MILLISECONDS);
    }
    
    public void explicitWait(WebDriver driver, int timeout, WebElement element){
        new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(element));
    }
}
