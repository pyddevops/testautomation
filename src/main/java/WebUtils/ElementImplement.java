package WebUtils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ElementImplement{

    WaitUtils webWait = new WaitUtils();
    
    /**
     * Click on the Mobile Element
     * @author neham
     * @param driver, timeout, element
     */
    public void click(WebDriver driver, int timeout, WebElement element) {
        webWait.explicitWait(driver, timeout, element);
        String script = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);\n" +
                "var elementTop = arguments[0].getBoundingClientRect().top;\n" +
                "window.scrollBy(0, elementTop-(viewPortHeight/2));";
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript(script, element);
        element.click();
    }

    /**
     * Write Text in the Mobile Text box
     * @author neham
     * @param driver, timeout, element, text
     */
    public void sendText(WebDriver driver, int timeout, WebElement element, CharSequence... text) {
        webWait.explicitWait(driver, timeout, element);
        element.clear();
        element.sendKeys(text);
    }

    /**
     * Select the value from Dropdown through 
     * @author neham    
     * @param xpath, text
     */
    public void selectValueFromDropdown(WebDriver driver, String xpath, String text) {
        webWait.implicitWait(driver, 60);
        WebElement listElement = driver.findElement(By.xpath(xpath));
        Select drpSelect = new Select(listElement);
        drpSelect.selectByVisibleText(text);

    }

    public void navigateToURL(WebDriver driver, String url){
        driver.navigate().to(url);
    }
}
