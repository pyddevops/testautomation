package WebUtils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class LogResults{
    
    public static String reportname;
    public static String intialReportname;
    public static String report;
    private static final Map<Long, ExtentTest> extentTestMap = new HashMap<>();
    static ExtentReports extent = getReporter("AutomationReport");
    
    public synchronized static ExtentReports getReporter(String className) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
        reportname = className + "." + sdf.format(new Timestamp(System.currentTimeMillis())) + ".html";
        final String filePath = reportname;
        report = filePath;
        if (extent == null) {
            extent = new ExtentReports(filePath, true);
            intialReportname = reportname;
        }
        return extent;
    }
    
    @SuppressWarnings("unchecked")
    public static ExtentTest startTest(String testName, String desc){
        ExtentTest test = extent.startTest(testName, desc);
        extentTestMap.put(Thread.currentThread().getId(), test);
        return extent.startTest(testName);
    }
  
    public static void endTest(){
        extent.endTest(extentTestMap.get(Thread.currentThread().getId()));
    }
    
    public static ExtentTest logStep(){
        return extentTestMap.get(Thread.currentThread().getId());
    }
    
}
