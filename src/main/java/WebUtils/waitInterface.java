package WebUtils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public interface waitInterface {


    public void implicitWait(WebDriver driver, int timeout);

    public void explicitWait(WebDriver driver, int timeout, WebElement element);


}
